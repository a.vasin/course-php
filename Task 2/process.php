<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Результат</title>
</head>
<body>
	<p>
	<?
	if (empty($_POST['text']) && empty($_FILES['file']['name'])) {
		echo("Пожалуйста, заполните хотя бы одно поле");
	} else {
		echo("Текст успешно обработан");
	}
	?>
	</p>
	<form action="/">
	    <button type="submit">На начальную страницу</button>
	</form>
</body>
</html>
<?
if (!empty($_POST['text'])) {
    $text = $_POST['text'];
	$cyrillic = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
	$symbols = ["-", "!", "?", ".", ",", ""];
	$text = str_replace($symbols , "", $text);
	$wordsArray = array_filter(explode(' ', $text));
	$countArray = array_count_values($wordsArray);
	$fp = fopen('textFromInput.csv', 'w');
	fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));
	foreach ($countArray as $key => $number) {
		fputcsv($fp, array($key, $number), ';');
	}
	$wordCount = str_word_count($text, 0, $cyrillic);
	fputcsv($fp, array("Всего слов:", $wordCount), ";");
	fclose($fp);
}
if (!empty($_FILES['file']['name'])) {
	$text = file_get_contents('./'.$_FILES['file']['name'], true);
	$text = iconv("windows-1251", "utf-8", $text);
	$cyrillic = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
	$symbols = ["-", "!", "?", ".", ",", ""];
	$text = str_replace($symbols , "", $text);
	$wordsArray = array_filter(explode(' ', $text));
	$countArray = array_count_values($wordsArray);
	$fp = fopen('textFromFile.csv', 'w');
	fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));
	foreach ($countArray as $key => $number) {
		fputcsv($fp, array($key, $number), ';');
	}
	$wordCount = str_word_count($text, 0, $cyrillic);
	fputcsv($fp, array("Всего слов:", $wordCount), ";");
	fclose($fp);
}
?>