<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Результат</title>
</head>
<style>
	table {
		border: 1px solid #1C6EA4;
		background-color: #EEEEEE;
		width: 100%;
		text-align: left;
		border-collapse: collapse;
	}
	table td, table th {
		border: 1px solid #AAAAAA;
		padding: 3px 2px;
	}
	table tbody td {
		font-size: 13px;
	}
	table tr:nth-child(even) {
		background: #D0E4F5;
	}
	table thead {
		background: #1C6EA4;
		background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
		background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
		background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
		border-bottom: 2px solid #444444;
	}
	table thead th {
		font-size: 15px;
		font-weight: bold;
		color: #FFFFFF;
		border-left: 2px solid #D0E4F5;
	}
	table thead th:first-child {
		border-left: none;
	}

	table tfoot {
		font-size: 14px;
		font-weight: bold;
		color: #FFFFFF;
		background: #D0E4F5;
		background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
		background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
		background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
		border-top: 2px solid #444444;
	}
	table tfoot td {
		font-size: 14px;
	}
	table tfoot {
		text-align: right;
	}
	table tfoot a{
		display: inline-block;
		background: #1C6EA4;
		color: #FFFFFF;
		padding: 2px 8px;
		border-radius: 5px;
	}
</style>
<body>
	<p>
	<?
	if (empty($_POST['text']) && empty($_FILES['file']['name'])) {
		echo("Пожалуйста, заполните хотя бы одно поле");
	} else {
		echo("Текст успешно обработан");
	}
	?>
	</p>
	<form action="/">
	    <button type="submit">На начальную страницу</button>
	</form>
</body>
</html>
<?
$hostname = 'localhost';
$username = 'root';
$password = '';
$database = 'task3';
$conn = mysqli_connect($hostname, $username, $password, $database);
if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
}
if (!empty($_POST['text'])) {
    $text = $_POST['text'];
	$cyrillic = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
	$symbols = ["-", "!", "?", ".", ",", ""];
	$text = str_replace($symbols , "", $text);
	$wordsArray = array_filter(explode(' ', $text));
	$countArray = array_count_values($wordsArray);
	$fp = fopen('textFromInput.csv', 'w');
	fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));
	echo("<br><br><table>
			<thead>
				<tr>
					<th>Слово</th>
					<th>Количество вхождений</th>
				</tr>
			</thead>");
	foreach ($countArray as $key => $number) {
		fputcsv($fp, array($key, $number), ';');
		$sql = "SELECT MAX(id) as max_id FROM total";
		$result = $conn -> query($sql);
		$row = $result -> fetch_assoc();
		$totalId = $row['max_id'] + 1;
		$sql = "INSERT INTO words (word, count, total_id) VALUES ('" . $key . "', '" . $number . "', " . $totalId . ")";
		try {
		    mysqli_query($conn, $sql);
		} catch (Exception $ex) {
			echo ("Ошибка: " . $ex->getMessage());
		}
		echo("<tr>
				<td>" . $key . "</td>
				<td>" . $number . "</td>
			</tr>");
	}
	echo("</tbody>
		</table>");
	$wordCount = str_word_count($text, 0, $cyrillic);
	fputcsv($fp, array("Всего слов:", $wordCount), ";");
	fclose($fp);
	echo ("<br><a href=\"./textFromInput.csv\">Ссылка на csv файл</a><br>");
	$sql = "INSERT INTO total (upload_date, all_text, count) VALUES ('" . date("Y-m-d") . "', '" . $_POST['text'] . "', '" . $wordCount . "')";
	try {
	    mysqli_query($conn, $sql);
	    echo (PHP_EOL."<br>Запись в базу данных из поля ввода произведена успешно");
	} catch (Exception $ex) {
		echo ("Ошибка: " . $ex->getMessage());
	}
	echo("<br><br><table>
			<thead>
				<tr>
					<th>Дата загрузки текста</th>
					<th>Текст</th>
					<th>Общее количество слов</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>" . date("Y-m-d") . "</td>
					<td>" . $_POST['text'] . "</td>
					<td>" . $wordCount . "</td>
				</tr>
			</tbody>
		</table>");
}
if (!empty($_FILES['file']['name'])) {
	$text = file_get_contents('./'.$_FILES['file']['name'], true);
	$text = iconv("windows-1251", "utf-8", $text);
	$cyrillic = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
	$symbols = ["-", "!", "?", ".", ",", ""];
	$text = str_replace($symbols , "", $text);
	$wordsArray = array_filter(explode(' ', $text));
	$countArray = array_count_values($wordsArray);
	$fp = fopen('textFromFile.csv', 'w');
	fputs($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));
	echo("<br><br><table>
			<thead>
				<tr>
					<th>Слово</th>
					<th>Количество вхождений</th>
				</tr>
			</thead>");
	foreach ($countArray as $key => $number) {
		fputcsv($fp, array($key, $number), ';');
		$sql = "SELECT MAX(id) as max_id FROM total";
		$result = $conn -> query($sql);
		$row = $result -> fetch_assoc();
		$totalId = $row['max_id'] + 1;
		$sql = "INSERT INTO words (word, count, total_id) VALUES ('" . $key . "', '" . $number . "', " . $totalId . ")";
		try {
		    mysqli_query($conn, $sql);
		} catch (Exception $ex) {
			echo ("Ошибка: " . $ex->getMessage());
		}
		echo("<tr>
				<td>" . $key . "</td>
				<td>" . $number . "</td>
			</tr>");
	}
	echo("</tbody>
		</table>");
	$wordCount = str_word_count($text, 0, $cyrillic);
	fputcsv($fp, array("Всего слов:", $wordCount), ";");
	fclose($fp);
	echo ("<br><a href=\"./textFromFile.csv\">Ссылка на csv файл</a><br>");
	$sql = "INSERT INTO total (upload_date, all_text, count) VALUES ('" . date("Y-m-d") . "', '" . $text . "', '" . $wordCount . "')";
	try {
	    mysqli_query($conn, $sql);
	    echo (PHP_EOL."<br>Запись в базу данных из файла произведена успешно");
	} catch (Exception $ex) {
		echo ("Ошибка: " . $ex->getMessage());
	}
	echo("<br><br><table>
			<thead>
				<tr>
					<th>Дата загрузки текста</th>
					<th>Текст</th>
					<th>Общее количество слов</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>" . date("Y-m-d") . "</td>
					<td>" . mb_strimwidth($text, 0, 150, "...") . "</td>
					<td>" . $wordCount . "</td>
				</tr>
			</tbody>
		</table>");
}
mysqli_close($conn);
?>